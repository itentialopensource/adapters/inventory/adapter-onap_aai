# ONAP - Active and Available Inventory

Vendor: ONAP
Homepage: https://www.onap.org/

Product: Active and Available Inventory
Product Page: https://wiki.onap.org/pages/viewpage.action?pageId=1015836

## Introduction
We classify ONAP Active and Available Inventory into the Inventory and Network Services domains as ONAP Active and Available Inventory provides information on Inventory and Topology. 

"AAI uses a central registry to create a global view of inventory and network topology." 

## Why Integrate
The ONAP Active and Available Inventory adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Active and Available Inventory DDI to offer inventory and topology for configuration and orchestration. 

With this adapter you have the ability to perform operations with ONAP Active and Available Inventory such as:

- Network
- Cloud Infrastructure

## Additional Product Documentation
The [ONAP AAI REST API](https://docs.onap.org/projects/onap-aai-aai-common/en/latest/AAI%20REST%20API%20Documentation/AAIRESTAPI.html)
The [AAI Architecture in ONAP](https://docs.onap.org/projects/onap-aai-aai-common/en/latest/platform/architecture.html)
