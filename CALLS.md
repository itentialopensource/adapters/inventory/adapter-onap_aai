## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ONAP Active and Available Inventory. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ONAP Active and Available Inventory.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ONAP - Active and Available Inventory. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureComplexesComplexCtagPoolsCtagPoolRelationshipListRelationship(physicalLocationId, targetPe, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureComplexesComplexCtagPoolsCtagPoolRelationshipListRelationship(physicalLocationId, targetPe, availabilityZoneName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureComplexesComplexCtagPoolsCtagPool(physicalLocationId, targetPe, availabilityZoneName, callback)</td>
    <td style="padding:15px">returns ctag-pool</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureComplexesComplexCtagPoolsCtagPool(physicalLocationId, targetPe, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">create or update an existing ctag-pool</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureComplexesComplexCtagPoolsCtagPool(physicalLocationId, targetPe, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">update an existing ctag-pool</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureComplexesComplexCtagPoolsCtagPool(physicalLocationId, targetPe, availabilityZoneName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing ctag-pool</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools/ctag-pool/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureComplexesComplexCtagPools(physicalLocationId, availabilityZoneName, callback)</td>
    <td style="padding:15px">returns ctag-pools</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/ctag-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureComplexesComplexRelationshipListRelationship(physicalLocationId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureComplexesComplexRelationshipListRelationship(physicalLocationId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureComplexesComplex(physicalLocationId, callback)</td>
    <td style="padding:15px">returns complex</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureComplexesComplex(physicalLocationId, body, callback)</td>
    <td style="padding:15px">create or update an existing complex</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureComplexesComplex(physicalLocationId, body, callback)</td>
    <td style="padding:15px">update an existing complex</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureComplexesComplex(physicalLocationId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing complex</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes/complex/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureComplexes(physicalLocationId, dataCenterCode, complexName, identityUrl, callback)</td>
    <td style="padding:15px">returns complexes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/complexes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroupRelationshipListRelationship(cloudOwner, cloudRegionId, volumeGroupId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroupRelationshipListRelationship(cloudOwner, cloudRegionId, volumeGroupId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroup(cloudOwner, cloudRegionId, volumeGroupId, callback)</td>
    <td style="padding:15px">returns volume-group</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroup(cloudOwner, cloudRegionId, volumeGroupId, body, callback)</td>
    <td style="padding:15px">create or update an existing volume-group</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroup(cloudOwner, cloudRegionId, volumeGroupId, body, callback)</td>
    <td style="padding:15px">update an existing volume-group</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVolumeGroupsVolumeGroup(cloudOwner, cloudRegionId, volumeGroupId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing volume-group</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups/volume-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionVolumeGroups(cloudOwner, cloudRegionId, volumeGroupId, volumeGroupName, heatStackId, vnfType, callback)</td>
    <td style="padding:15px">returns volume-groups</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/volume-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolumeRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolumeRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolume(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, callback)</td>
    <td style="padding:15px">returns volume</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolume(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, body, callback)</td>
    <td style="padding:15px">create or update an existing volume</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolume(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, body, callback)</td>
    <td style="padding:15px">update an existing volume</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumesVolume(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing volume</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes/volume/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverVolumes(cloudOwner, cloudRegionId, tenantId, vserverId, volumeId, callback)</td>
    <td style="padding:15px">returns volumes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv4-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}/l3-interface-ipv6-address-list/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlan(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlan(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlan(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlansVlan(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans/vlan/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceVlans(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVf(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVf(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVf(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfsSriovVf(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs/sriov-vf/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceSriovVfs(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv4AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv4-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterfaceL3InterfaceIpv6AddressList(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}/l3-interface-ipv6-address-list/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterface(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterface(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterface(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfacesLInterface(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces/l-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserverLInterfaces(cloudOwner, cloudRegionId, tenantId, vserverId, interfaceName, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserver(cloudOwner, cloudRegionId, tenantId, vserverId, callback)</td>
    <td style="padding:15px">returns vserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserver(cloudOwner, cloudRegionId, tenantId, vserverId, body, callback)</td>
    <td style="padding:15px">create or update an existing vserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserver(cloudOwner, cloudRegionId, tenantId, vserverId, body, callback)</td>
    <td style="padding:15px">update an existing vserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVserversVserver(cloudOwner, cloudRegionId, tenantId, vserverId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers/vserver/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantVservers(cloudOwner, cloudRegionId, tenantId, vserverId, vserverName, vserverName2, provStatus, inMaint, isClosedLoopDisabled, callback)</td>
    <td style="padding:15px">returns vservers</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/vservers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServerRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, nosServerId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServerRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, nosServerId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServer(cloudOwner, cloudRegionId, tenantId, nosServerId, callback)</td>
    <td style="padding:15px">returns nos-server</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServer(cloudOwner, cloudRegionId, tenantId, nosServerId, body, callback)</td>
    <td style="padding:15px">create or update an existing nos-server</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServer(cloudOwner, cloudRegionId, tenantId, nosServerId, body, callback)</td>
    <td style="padding:15px">update an existing nos-server</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServersNosServer(cloudOwner, cloudRegionId, tenantId, nosServerId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing nos-server</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers/nos-server/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenantNosServers(cloudOwner, cloudRegionId, tenantId, nosServerId, callback)</td>
    <td style="padding:15px">returns nos-servers</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/nos-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenantRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenantRelationshipListRelationship(cloudOwner, cloudRegionId, tenantId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenantsTenant(cloudOwner, cloudRegionId, tenantId, callback)</td>
    <td style="padding:15px">returns tenant</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionTenantsTenant(cloudOwner, cloudRegionId, tenantId, body, callback)</td>
    <td style="padding:15px">create or update an existing tenant</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionTenantsTenant(cloudOwner, cloudRegionId, tenantId, body, callback)</td>
    <td style="padding:15px">update an existing tenant</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionTenantsTenant(cloudOwner, cloudRegionId, tenantId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing tenant</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants/tenant/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionTenants(cloudOwner, cloudRegionId, tenantId, tenantName, tenantContext, callback)</td>
    <td style="padding:15px">returns tenants</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributesRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributesRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, callback)</td>
    <td style="padding:15px">returns hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">create or update an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">update an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaAttributeKey, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}/hpa-feature-attributes/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, callback)</td>
    <td style="padding:15px">returns hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">create or update an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">update an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities/hpa-capability/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorHpaCapabilities(cloudOwner, cloudRegionId, flavorId, hpaCapabilityId, hpaFeature, architecture, callback)</td>
    <td style="padding:15px">returns hpa-capabilities</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/hpa-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavorRelationshipListRelationship(cloudOwner, cloudRegionId, flavorId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavor(cloudOwner, cloudRegionId, flavorId, callback)</td>
    <td style="padding:15px">returns flavor</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavor(cloudOwner, cloudRegionId, flavorId, body, callback)</td>
    <td style="padding:15px">create or update an existing flavor</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavor(cloudOwner, cloudRegionId, flavorId, body, callback)</td>
    <td style="padding:15px">update an existing flavor</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionFlavorsFlavor(cloudOwner, cloudRegionId, flavorId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing flavor</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors/flavor/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionFlavors(cloudOwner, cloudRegionId, flavorId, flavorName, callback)</td>
    <td style="padding:15px">returns flavors</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/flavors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignmentRelationshipListRelationship(cloudOwner, cloudRegionId, groupId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignmentRelationshipListRelationship(cloudOwner, cloudRegionId, groupId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignment(cloudOwner, cloudRegionId, groupId, callback)</td>
    <td style="padding:15px">returns group-assignment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignment(cloudOwner, cloudRegionId, groupId, body, callback)</td>
    <td style="padding:15px">create or update an existing group-assignment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignment(cloudOwner, cloudRegionId, groupId, body, callback)</td>
    <td style="padding:15px">update an existing group-assignment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionGroupAssignmentsGroupAssignment(cloudOwner, cloudRegionId, groupId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing group-assignment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments/group-assignment/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionGroupAssignments(cloudOwner, cloudRegionId, groupId, groupType, groupName, callback)</td>
    <td style="padding:15px">returns group-assignments</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/group-assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshotRelationshipListRelationship(cloudOwner, cloudRegionId, snapshotId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshotRelationshipListRelationship(cloudOwner, cloudRegionId, snapshotId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshot(cloudOwner, cloudRegionId, snapshotId, callback)</td>
    <td style="padding:15px">returns snapshot</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshot(cloudOwner, cloudRegionId, snapshotId, body, callback)</td>
    <td style="padding:15px">create or update an existing snapshot</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshot(cloudOwner, cloudRegionId, snapshotId, body, callback)</td>
    <td style="padding:15px">update an existing snapshot</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionSnapshotsSnapshot(cloudOwner, cloudRegionId, snapshotId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing snapshot</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots/snapshot/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionSnapshots(cloudOwner, cloudRegionId, snapshotId, snapshotName, application, applicationVendor, applicationVersion, prevSnapshotId, callback)</td>
    <td style="padding:15px">returns snapshots</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionImagesImageRelationshipListRelationship(cloudOwner, cloudRegionId, imageId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionImagesImageRelationshipListRelationship(cloudOwner, cloudRegionId, imageId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionImagesImageMetadataMetadatum(cloudOwner, cloudRegionId, imageId, metaname, callback)</td>
    <td style="padding:15px">returns metadatum</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionImagesImageMetadataMetadatum(cloudOwner, cloudRegionId, imageId, metaname, body, callback)</td>
    <td style="padding:15px">create or update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionImagesImageMetadataMetadatum(cloudOwner, cloudRegionId, imageId, metaname, body, callback)</td>
    <td style="padding:15px">update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionImagesImageMetadataMetadatum(cloudOwner, cloudRegionId, imageId, metaname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionImagesImageMetadata(cloudOwner, cloudRegionId, imageId, metaname, callback)</td>
    <td style="padding:15px">returns metadata</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionImagesImage(cloudOwner, cloudRegionId, imageId, callback)</td>
    <td style="padding:15px">returns image</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionImagesImage(cloudOwner, cloudRegionId, imageId, body, callback)</td>
    <td style="padding:15px">create or update an existing image</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionImagesImage(cloudOwner, cloudRegionId, imageId, body, callback)</td>
    <td style="padding:15px">update an existing image</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionImagesImage(cloudOwner, cloudRegionId, imageId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing image</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images/image/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionImages(cloudOwner, cloudRegionId, imageId, imageName, application, applicationVendor, applicationVersion, callback)</td>
    <td style="padding:15px">returns images</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitchRelationshipListRelationship(cloudOwner, cloudRegionId, switchName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitchRelationshipListRelationship(cloudOwner, cloudRegionId, switchName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitch(cloudOwner, cloudRegionId, switchName, callback)</td>
    <td style="padding:15px">returns dvs-switch</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitch(cloudOwner, cloudRegionId, switchName, body, callback)</td>
    <td style="padding:15px">create or update an existing dvs-switch</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitch(cloudOwner, cloudRegionId, switchName, body, callback)</td>
    <td style="padding:15px">update an existing dvs-switch</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionDvsSwitchesDvsSwitch(cloudOwner, cloudRegionId, switchName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing dvs-switch</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches/dvs-switch/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionDvsSwitches(cloudOwner, cloudRegionId, switchName, vcenterUrl, callback)</td>
    <td style="padding:15px">returns dvs-switches</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/dvs-switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetworkRelationshipListRelationship(cloudOwner, cloudRegionId, networkUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetworkRelationshipListRelationship(cloudOwner, cloudRegionId, networkUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetwork(cloudOwner, cloudRegionId, networkUuid, callback)</td>
    <td style="padding:15px">returns oam-network</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetwork(cloudOwner, cloudRegionId, networkUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing oam-network</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetwork(cloudOwner, cloudRegionId, networkUuid, body, callback)</td>
    <td style="padding:15px">update an existing oam-network</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionOamNetworksOamNetwork(cloudOwner, cloudRegionId, networkUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing oam-network</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks/oam-network/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionOamNetworks(cloudOwner, cloudRegionId, networkUuid, networkName, cvlanTag, callback)</td>
    <td style="padding:15px">returns oam-networks</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/oam-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZoneRelationshipListRelationship(cloudOwner, cloudRegionId, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZoneRelationshipListRelationship(cloudOwner, cloudRegionId, availabilityZoneName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZone(cloudOwner, cloudRegionId, availabilityZoneName, callback)</td>
    <td style="padding:15px">returns availability-zone</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZone(cloudOwner, cloudRegionId, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">create or update an existing availability-zone</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZone(cloudOwner, cloudRegionId, availabilityZoneName, body, callback)</td>
    <td style="padding:15px">update an existing availability-zone</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionAvailabilityZonesAvailabilityZone(cloudOwner, cloudRegionId, availabilityZoneName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing availability-zone</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones/availability-zone/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionAvailabilityZones(cloudOwner, cloudRegionId, availabilityZoneName, callback)</td>
    <td style="padding:15px">returns availability-zones</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/availability-zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionRelationshipListRelationship(cloudOwner, cloudRegionId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionRelationshipListRelationship(cloudOwner, cloudRegionId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, vipIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, vipIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressList(cloudOwner, cloudRegionId, vipIpv4Address, callback)</td>
    <td style="padding:15px">returns vip-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressList(cloudOwner, cloudRegionId, vipIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing vip-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressList(cloudOwner, cloudRegionId, vipIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing vip-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVipIpv4AddressList(cloudOwner, cloudRegionId, vipIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vip-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, vipIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressListRelationshipListRelationship(cloudOwner, cloudRegionId, vipIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressList(cloudOwner, cloudRegionId, vipIpv6Address, callback)</td>
    <td style="padding:15px">returns vip-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressList(cloudOwner, cloudRegionId, vipIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing vip-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressList(cloudOwner, cloudRegionId, vipIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing vip-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionVipIpv6AddressList(cloudOwner, cloudRegionId, vipIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vip-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/vip-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityRelationshipListRelationship(cloudOwner, cloudRegionId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityRelationshipListRelationship(cloudOwner, cloudRegionId, hpaCapabilityId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributesRelationshipListRelationship(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributesRelationshipListRelationship(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, callback)</td>
    <td style="padding:15px">returns hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">create or update an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, body, callback)</td>
    <td style="padding:15px">update an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapabilityHpaFeatureAttributes(cloudOwner, cloudRegionId, hpaCapabilityId, hpaAttributeKey, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing hpa-feature-attributes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}/hpa-feature-attributes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, hpaCapabilityId, callback)</td>
    <td style="padding:15px">returns hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">create or update an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, hpaCapabilityId, body, callback)</td>
    <td style="padding:15px">update an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionHpaCapabilitiesHpaCapability(cloudOwner, cloudRegionId, hpaCapabilityId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing hpa-capability</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities/hpa-capability/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionHpaCapabilities(cloudOwner, cloudRegionId, hpaCapabilityId, hpaFeature, architecture, callback)</td>
    <td style="padding:15px">returns hpa-capabilities</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/hpa-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(cloudOwner, cloudRegionId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(cloudOwner, cloudRegionId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfo(cloudOwner, cloudRegionId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfo(cloudOwner, cloudRegionId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfo(cloudOwner, cloudRegionId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoListEsrSystemInfo(cloudOwner, cloudRegionId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list/esr-system-info/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegionEsrSystemInfoList(cloudOwner, cloudRegionId, esrSystemInfoId, systemName, systemType, callback)</td>
    <td style="padding:15px">returns esr-system-info-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}/esr-system-info-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegionsCloudRegion(cloudOwner, cloudRegionId, callback)</td>
    <td style="padding:15px">returns cloud-region</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureCloudRegionsCloudRegion(cloudOwner, cloudRegionId, body, callback)</td>
    <td style="padding:15px">create or update an existing cloud-region</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureCloudRegionsCloudRegion(cloudOwner, cloudRegionId, body, callback)</td>
    <td style="padding:15px">update an existing cloud-region</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureCloudRegionsCloudRegion(cloudOwner, cloudRegionId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing cloud-region</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions/cloud-region/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureCloudRegions(cloudOwner, cloudRegionId, cloudType, ownerDefinedType, callback)</td>
    <td style="padding:15px">returns cloud-regions</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/cloud-regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureNetworkProfilesNetworkProfileRelationshipListRelationship(nmProfileName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureNetworkProfilesNetworkProfileRelationshipListRelationship(nmProfileName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureNetworkProfilesNetworkProfile(nmProfileName, callback)</td>
    <td style="padding:15px">returns network-profile</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureNetworkProfilesNetworkProfile(nmProfileName, body, callback)</td>
    <td style="padding:15px">create or update an existing network-profile</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureNetworkProfilesNetworkProfile(nmProfileName, body, callback)</td>
    <td style="padding:15px">update an existing network-profile</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureNetworkProfilesNetworkProfile(nmProfileName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing network-profile</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureNetworkProfiles(nmProfileName, callback)</td>
    <td style="padding:15px">returns network-profiles</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverRelationshipListRelationship(hostname, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverRelationshipListRelationship(hostname, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceRelationshipListRelationship(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceRelationshipListRelationship(hostname, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(hostname, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(hostname, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPf(hostname, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPf(hostname, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPf(hostname, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfsSriovPf(hostname, interfaceName, pfPciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceSriovPfs(hostname, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pfs</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceVlans(hostname, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceSriovVfs(hostname, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterfaceLInterfaces(hostname, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfacesPInterface(hostname, interfaceName, callback)</td>
    <td style="padding:15px">returns p-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverPInterfacesPInterface(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverPInterfacesPInterface(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverPInterfacesPInterface(hostname, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverPInterfaces(hostname, interfaceName, provStatus, operationalStatus, callback)</td>
    <td style="padding:15px">returns p-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/p-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceRelationshipListRelationship(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceRelationshipListRelationship(hostname, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(hostname, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceVlans(hostname, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(hostname, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfs(hostname, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(hostname, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfacesLInterface(hostname, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterfaceLInterfaces(hostname, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfacesLagInterface(hostname, interfaceName, callback)</td>
    <td style="padding:15px">returns lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverLagInterfacesLagInterface(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverLagInterfacesLagInterface(hostname, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverLagInterfacesLagInterface(hostname, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverLagInterfaces(hostname, interfaceName, interfaceId, interfaceRole, callback)</td>
    <td style="padding:15px">returns lag-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/lag-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverHpaCapacityRelationshipListRelationship(hostname, hpaCapacityKey, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverHpaCapacityRelationshipListRelationship(hostname, hpaCapacityKey, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserverHpaCapacity(hostname, hpaCapacityKey, callback)</td>
    <td style="padding:15px">returns hpa-capacity</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserverHpaCapacity(hostname, hpaCapacityKey, body, callback)</td>
    <td style="padding:15px">create or update an existing hpa-capacity</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserverHpaCapacity(hostname, hpaCapacityKey, body, callback)</td>
    <td style="padding:15px">update an existing hpa-capacity</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserverHpaCapacity(hostname, hpaCapacityKey, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing hpa-capacity</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}/hpa-capacity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePserversPserver(hostname, callback)</td>
    <td style="padding:15px">returns pserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructurePserversPserver(hostname, body, callback)</td>
    <td style="padding:15px">create or update an existing pserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructurePserversPserver(hostname, body, callback)</td>
    <td style="padding:15px">update an existing pserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructurePserversPserver(hostname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing pserver</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers/pserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructurePservers(hostname, invStatus, pserverId, inMaint, pserverName2, callback)</td>
    <td style="padding:15px">returns pservers</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/pservers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureVirtualDataCentersVirtualDataCenterRelationshipListRelationship(vdcId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureVirtualDataCentersVirtualDataCenterRelationshipListRelationship(vdcId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureVirtualDataCentersVirtualDataCenter(vdcId, callback)</td>
    <td style="padding:15px">returns virtual-data-center</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureVirtualDataCentersVirtualDataCenter(vdcId, body, callback)</td>
    <td style="padding:15px">create or update an existing virtual-data-center</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureVirtualDataCentersVirtualDataCenter(vdcId, body, callback)</td>
    <td style="padding:15px">update an existing virtual-data-center</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureVirtualDataCentersVirtualDataCenter(vdcId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing virtual-data-center</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers/virtual-data-center/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureVirtualDataCenters(vdcId, vdcName, callback)</td>
    <td style="padding:15px">returns virtual-data-centers</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/virtual-data-centers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureOperationalEnvironmentsOperationalEnvironmentRelationshipListRelationship(operationalEnvironmentId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureOperationalEnvironmentsOperationalEnvironmentRelationshipListRelationship(operationalEnvironmentId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureOperationalEnvironmentsOperationalEnvironment(operationalEnvironmentId, callback)</td>
    <td style="padding:15px">returns operational-environment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureOperationalEnvironmentsOperationalEnvironment(operationalEnvironmentId, body, callback)</td>
    <td style="padding:15px">create or update an existing operational-environment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureOperationalEnvironmentsOperationalEnvironment(operationalEnvironmentId, body, callback)</td>
    <td style="padding:15px">update an existing operational-environment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureOperationalEnvironmentsOperationalEnvironment(operationalEnvironmentId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing operational-environment</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments/operational-environment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureOperationalEnvironments(operationalEnvironmentId, callback)</td>
    <td style="padding:15px">returns operational-environments</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/operational-environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureNetworkTechnologiesNetworkTechnologyRelationshipListRelationship(networkTechnologyId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureNetworkTechnologiesNetworkTechnologyRelationshipListRelationship(networkTechnologyId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureNetworkTechnologiesNetworkTechnology(networkTechnologyId, callback)</td>
    <td style="padding:15px">returns network-technology</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCloudInfrastructureNetworkTechnologiesNetworkTechnology(networkTechnologyId, body, callback)</td>
    <td style="padding:15px">create or update an existing network-technology</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInfrastructureNetworkTechnologiesNetworkTechnology(networkTechnologyId, body, callback)</td>
    <td style="padding:15px">update an existing network-technology</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructureNetworkTechnologiesNetworkTechnology(networkTechnologyId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing network-technology</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies/network-technology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructureNetworkTechnologies(callback)</td>
    <td style="padding:15px">returns network-technologies</td>
    <td style="padding:15px">{base_path}/{version}/cloud-infrastructure/network-technologies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(emsId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(emsId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfo(emsId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfo(emsId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfo(emsId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrEmsListEsrEmsEsrSystemInfoListEsrSystemInfo(emsId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrEmsListEsrEmsEsrSystemInfoList(emsId, esrSystemInfoId, systemName, systemType, callback)</td>
    <td style="padding:15px">returns esr-system-info-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/esr-system-info-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrEmsListEsrEmsRelationshipListRelationship(emsId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrEmsListEsrEmsRelationshipListRelationship(emsId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrEmsListEsrEms(emsId, callback)</td>
    <td style="padding:15px">returns esr-ems</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrEmsListEsrEms(emsId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-ems</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrEmsListEsrEms(emsId, body, callback)</td>
    <td style="padding:15px">update an existing esr-ems</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrEmsListEsrEms(emsId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-ems</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list/esr-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrEmsList(emsId, callback)</td>
    <td style="padding:15px">returns esr-ems-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-ems-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(vnfmId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(vnfmId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfo(vnfmId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfo(vnfmId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfo(vnfmId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoListEsrSystemInfo(vnfmId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrVnfmListEsrVnfmEsrSystemInfoList(vnfmId, esrSystemInfoId, systemName, systemType, callback)</td>
    <td style="padding:15px">returns esr-system-info-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/esr-system-info-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrVnfmListEsrVnfmRelationshipListRelationship(vnfmId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrVnfmListEsrVnfmRelationshipListRelationship(vnfmId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrVnfmListEsrVnfm(vnfmId, callback)</td>
    <td style="padding:15px">returns esr-vnfm</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrVnfmListEsrVnfm(vnfmId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-vnfm</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrVnfmListEsrVnfm(vnfmId, body, callback)</td>
    <td style="padding:15px">update an existing esr-vnfm</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrVnfmListEsrVnfm(vnfmId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-vnfm</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list/esr-vnfm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrVnfmList(vnfmId, callback)</td>
    <td style="padding:15px">returns esr-vnfm-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-vnfm-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(nfvoId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(nfvoId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfo(nfvoId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfo(nfvoId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfo(nfvoId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoListEsrSystemInfo(nfvoId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrNfvoListEsrNfvoEsrSystemInfoList(nfvoId, esrSystemInfoId, systemName, systemType, callback)</td>
    <td style="padding:15px">returns esr-system-info-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/esr-system-info-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrNfvoListEsrNfvoRelationshipListRelationship(nfvoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrNfvoListEsrNfvoRelationshipListRelationship(nfvoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrNfvoListEsrNfvo(nfvoId, callback)</td>
    <td style="padding:15px">returns esr-nfvo</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrNfvoListEsrNfvo(nfvoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-nfvo</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrNfvoListEsrNfvo(nfvoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-nfvo</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrNfvoListEsrNfvo(nfvoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-nfvo</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list/esr-nfvo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrNfvoList(nfvoId, callback)</td>
    <td style="padding:15px">returns esr-nfvo-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-nfvo-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(thirdpartySdncId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfoRelationshipListRelationship(thirdpartySdncId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfo(thirdpartySdncId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfo(thirdpartySdncId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfo(thirdpartySdncId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoListEsrSystemInfo(thirdpartySdncId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrThirdpartySdncListEsrThirdpartySdncEsrSystemInfoList(thirdpartySdncId, esrSystemInfoId, systemName, systemType, callback)</td>
    <td style="padding:15px">returns esr-system-info-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/esr-system-info-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrThirdpartySdncListEsrThirdpartySdncRelationshipListRelationship(thirdpartySdncId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrThirdpartySdncListEsrThirdpartySdncRelationshipListRelationship(thirdpartySdncId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrThirdpartySdncListEsrThirdpartySdnc(thirdpartySdncId, callback)</td>
    <td style="padding:15px">returns esr-thirdparty-sdnc</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateExternalSystemEsrThirdpartySdncListEsrThirdpartySdnc(thirdpartySdncId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-thirdparty-sdnc</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalSystemEsrThirdpartySdncListEsrThirdpartySdnc(thirdpartySdncId, body, callback)</td>
    <td style="padding:15px">update an existing esr-thirdparty-sdnc</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalSystemEsrThirdpartySdncListEsrThirdpartySdnc(thirdpartySdncId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-thirdparty-sdnc</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list/esr-thirdparty-sdnc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSystemEsrThirdpartySdncList(callback)</td>
    <td style="padding:15px">returns esr-thirdparty-sdnc-list</td>
    <td style="padding:15px">{base_path}/{version}/external-system/esr-thirdparty-sdnc-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessConnectorsConnectorRelationshipListRelationship(resourceInstanceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessConnectorsConnectorRelationshipListRelationship(resourceInstanceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessConnectorsConnectorMetadataMetadatum(resourceInstanceId, metaname, callback)</td>
    <td style="padding:15px">returns metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessConnectorsConnectorMetadataMetadatum(resourceInstanceId, metaname, body, callback)</td>
    <td style="padding:15px">create or update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessConnectorsConnectorMetadataMetadatum(resourceInstanceId, metaname, body, callback)</td>
    <td style="padding:15px">update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessConnectorsConnectorMetadataMetadatum(resourceInstanceId, metaname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessConnectorsConnectorMetadata(resourceInstanceId, metaname, callback)</td>
    <td style="padding:15px">returns metadata</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessConnectorsConnector(resourceInstanceId, callback)</td>
    <td style="padding:15px">returns connector</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessConnectorsConnector(resourceInstanceId, body, callback)</td>
    <td style="padding:15px">create or update an existing connector</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessConnectorsConnector(resourceInstanceId, body, callback)</td>
    <td style="padding:15px">update an existing connector</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessConnectorsConnector(resourceInstanceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing connector</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors/connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessConnectors(resourceInstanceId, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, callback)</td>
    <td style="padding:15px">returns connectors</td>
    <td style="padding:15px">{base_path}/{version}/business/connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceMetadataMetadatum(globalCustomerId, serviceType, serviceInstanceId, metaname, callback)</td>
    <td style="padding:15px">returns metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceMetadataMetadatum(globalCustomerId, serviceType, serviceInstanceId, metaname, body, callback)</td>
    <td style="padding:15px">create or update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceMetadataMetadatum(globalCustomerId, serviceType, serviceInstanceId, metaname, body, callback)</td>
    <td style="padding:15px">update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceMetadataMetadatum(globalCustomerId, serviceType, serviceInstanceId, metaname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/metadata/metadatum/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceMetadata(globalCustomerId, serviceType, serviceInstanceId, metaname, callback)</td>
    <td style="padding:15px">returns metadata</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnectRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, id, idPath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnectRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, id, idPath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnect(globalCustomerId, serviceType, serviceInstanceId, id, idPath, callback)</td>
    <td style="padding:15px">returns tunnel-xconnect</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnect(globalCustomerId, serviceType, serviceInstanceId, id, idPath, body, callback)</td>
    <td style="padding:15px">create or update an existing tunnel-xconnect</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnect(globalCustomerId, serviceType, serviceInstanceId, id, idPath, body, callback)</td>
    <td style="padding:15px">update an existing tunnel-xconnect</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnectsTunnelXconnect(globalCustomerId, serviceType, serviceInstanceId, id, idPath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing tunnel-xconnect</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects/tunnel-xconnect/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceTunnelXconnects(globalCustomerId, serviceType, serviceInstanceId, id, idQuery, callback)</td>
    <td style="padding:15px">returns tunnel-xconnects</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/tunnel-xconnects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, id, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResourceRelationshipListRelationship(globalCustomerId, serviceType, serviceInstanceId, id, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResource(globalCustomerId, serviceType, serviceInstanceId, id, callback)</td>
    <td style="padding:15px">returns allotted-resource</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResource(globalCustomerId, serviceType, serviceInstanceId, id, body, callback)</td>
    <td style="padding:15px">create or update an existing allotted-resource</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResource(globalCustomerId, serviceType, serviceInstanceId, id, body, callback)</td>
    <td style="padding:15px">update an existing allotted-resource</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResourcesAllottedResource(globalCustomerId, serviceType, serviceInstanceId, id, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing allotted-resource</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources/allotted-resource/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstanceAllottedResources(globalCustomerId, serviceType, serviceInstanceId, id, modelInvariantId, modelVersionId, type, role, allottedResourceName, accessProviderId, accessClientId, accessTopologyId, accessNodeId, accessLtpId, vpnName, callback)</td>
    <td style="padding:15px">returns allotted-resources</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}/allotted-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstance(globalCustomerId, serviceType, serviceInstanceId, callback)</td>
    <td style="padding:15px">returns service-instance</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstance(globalCustomerId, serviceType, serviceInstanceId, body, callback)</td>
    <td style="padding:15px">create or update an existing service-instance</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstance(globalCustomerId, serviceType, serviceInstanceId, body, callback)</td>
    <td style="padding:15px">update an existing service-instance</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstancesServiceInstance(globalCustomerId, serviceType, serviceInstanceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing service-instance</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances/service-instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionServiceInstances(globalCustomerId, serviceType, serviceInstanceId, serviceInstanceName, environmentContext, workloadContext, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, serviceInstanceLocationId, orchestrationStatus, callback)</td>
    <td style="padding:15px">returns service-instances</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/service-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionRelationshipListRelationship(globalCustomerId, serviceType, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscriptionRelationshipListRelationship(globalCustomerId, serviceType, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptionsServiceSubscription(globalCustomerId, serviceType, callback)</td>
    <td style="padding:15px">returns service-subscription</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerServiceSubscriptionsServiceSubscription(globalCustomerId, serviceType, body, callback)</td>
    <td style="padding:15px">create or update an existing service-subscription</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomerServiceSubscriptionsServiceSubscription(globalCustomerId, serviceType, body, callback)</td>
    <td style="padding:15px">update an existing service-subscription</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerServiceSubscriptionsServiceSubscription(globalCustomerId, serviceType, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing service-subscription</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions/service-subscription/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomerServiceSubscriptions(globalCustomerId, serviceType, callback)</td>
    <td style="padding:15px">returns service-subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/service-subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomerRelationshipListRelationship(globalCustomerId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomerRelationshipListRelationship(globalCustomerId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomersCustomer(globalCustomerId, callback)</td>
    <td style="padding:15px">returns customer</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessCustomersCustomer(globalCustomerId, body, callback)</td>
    <td style="padding:15px">create or update an existing customer</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessCustomersCustomer(globalCustomerId, body, callback)</td>
    <td style="padding:15px">update an existing customer</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessCustomersCustomer(globalCustomerId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing customer</td>
    <td style="padding:15px">{base_path}/{version}/business/customers/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessCustomers(globalCustomerId, subscriberName, subscriberType, callback)</td>
    <td style="padding:15px">returns customers</td>
    <td style="padding:15px">{base_path}/{version}/business/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessLinesOfBusinessLineOfBusinessRelationshipListRelationship(lineOfBusinessName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessLinesOfBusinessLineOfBusinessRelationshipListRelationship(lineOfBusinessName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessLinesOfBusinessLineOfBusiness(lineOfBusinessName, callback)</td>
    <td style="padding:15px">returns line-of-business</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessLinesOfBusinessLineOfBusiness(lineOfBusinessName, body, callback)</td>
    <td style="padding:15px">create or update an existing line-of-business</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessLinesOfBusinessLineOfBusiness(lineOfBusinessName, body, callback)</td>
    <td style="padding:15px">update an existing line-of-business</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessLinesOfBusinessLineOfBusiness(lineOfBusinessName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing line-of-business</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business/line-of-business/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessLinesOfBusiness(lineOfBusinessName, callback)</td>
    <td style="padding:15px">returns lines-of-business</td>
    <td style="padding:15px">{base_path}/{version}/business/lines-of-business?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessOwningEntitiesOwningEntityRelationshipListRelationship(owningEntityId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessOwningEntitiesOwningEntityRelationshipListRelationship(owningEntityId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessOwningEntitiesOwningEntity(owningEntityId, callback)</td>
    <td style="padding:15px">returns owning-entity</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessOwningEntitiesOwningEntity(owningEntityId, body, callback)</td>
    <td style="padding:15px">create or update an existing owning-entity</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessOwningEntitiesOwningEntity(owningEntityId, body, callback)</td>
    <td style="padding:15px">update an existing owning-entity</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessOwningEntitiesOwningEntity(owningEntityId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing owning-entity</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities/owning-entity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessOwningEntities(owningEntityId, owningEntityName, callback)</td>
    <td style="padding:15px">returns owning-entities</td>
    <td style="padding:15px">{base_path}/{version}/business/owning-entities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessPlatformsPlatformRelationshipListRelationship(platformName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessPlatformsPlatformRelationshipListRelationship(platformName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessPlatformsPlatform(platformName, callback)</td>
    <td style="padding:15px">returns platform</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessPlatformsPlatform(platformName, body, callback)</td>
    <td style="padding:15px">create or update an existing platform</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessPlatformsPlatform(platformName, body, callback)</td>
    <td style="padding:15px">update an existing platform</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessPlatformsPlatform(platformName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing platform</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms/platform/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessPlatforms(platformName, callback)</td>
    <td style="padding:15px">returns platforms</td>
    <td style="padding:15px">{base_path}/{version}/business/platforms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessProjectsProjectRelationshipListRelationship(projectName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessProjectsProjectRelationshipListRelationship(projectName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessProjectsProject(projectName, callback)</td>
    <td style="padding:15px">returns project</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessProjectsProject(projectName, body, callback)</td>
    <td style="padding:15px">create or update an existing project</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessProjectsProject(projectName, body, callback)</td>
    <td style="padding:15px">update an existing project</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessProjectsProject(projectName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing project</td>
    <td style="padding:15px">{base_path}/{version}/business/projects/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessProjects(projectName, callback)</td>
    <td style="padding:15px">returns projects</td>
    <td style="padding:15px">{base_path}/{version}/business/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessSpPartnersSpPartnerRelationshipListRelationship(spPartnerId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessSpPartnersSpPartnerRelationshipListRelationship(spPartnerId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessSpPartnersSpPartner(spPartnerId, callback)</td>
    <td style="padding:15px">returns sp-partner</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateBusinessSpPartnersSpPartner(spPartnerId, body, callback)</td>
    <td style="padding:15px">create or update an existing sp-partner</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBusinessSpPartnersSpPartner(spPartnerId, body, callback)</td>
    <td style="padding:15px">update an existing sp-partner</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBusinessSpPartnersSpPartner(spPartnerId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sp-partner</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners/sp-partner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessSpPartners(spPartnerId, url, callsource, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns sp-partners</td>
    <td style="padding:15px">{base_path}/{version}/business/sp-partners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationVnfImagesVnfImageRelationshipListRelationship(vnfImageUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationVnfImagesVnfImageRelationshipListRelationship(vnfImageUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationVnfImagesVnfImage(vnfImageUuid, callback)</td>
    <td style="padding:15px">returns vnf-image</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationVnfImagesVnfImage(vnfImageUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing vnf-image</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationVnfImagesVnfImage(vnfImageUuid, body, callback)</td>
    <td style="padding:15px">update an existing vnf-image</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationVnfImagesVnfImage(vnfImageUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vnf-image</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images/vnf-image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationVnfImages(vnfImageUuid, application, applicationVendor, applicationVersion, callback)</td>
    <td style="padding:15px">returns vnf-images</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/vnf-images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationServicesServiceRelationshipListRelationship(serviceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationServicesServiceRelationshipListRelationship(serviceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationServicesService(serviceId, callback)</td>
    <td style="padding:15px">returns service</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationServicesService(serviceId, body, callback)</td>
    <td style="padding:15px">create or update an existing service</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationServicesService(serviceId, body, callback)</td>
    <td style="padding:15px">update an existing service</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationServicesService(serviceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing service</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationServices(serviceId, serviceDescription, callback)</td>
    <td style="padding:15px">returns services</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationServiceCapabilitiesServiceCapabilityRelationshipListRelationship(serviceType, vnfType, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationServiceCapabilitiesServiceCapabilityRelationshipListRelationship(serviceType, vnfType, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationServiceCapabilitiesServiceCapability(serviceType, vnfType, callback)</td>
    <td style="padding:15px">returns service-capability</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationServiceCapabilitiesServiceCapability(serviceType, vnfType, body, callback)</td>
    <td style="padding:15px">create or update an existing service-capability</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationServiceCapabilitiesServiceCapability(serviceType, vnfType, body, callback)</td>
    <td style="padding:15px">update an existing service-capability</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationServiceCapabilitiesServiceCapability(serviceType, vnfType, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing service-capability</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities/service-capability/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationServiceCapabilities(serviceType, vnfType, callback)</td>
    <td style="padding:15px">returns service-capabilities</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/service-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSetRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSetRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, callback)</td>
    <td style="padding:15px">returns element-choice-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing element-choice-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, body, callback)</td>
    <td style="padding:15px">update an existing element-choice-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSetsElementChoiceSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing element-choice-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets/element-choice-set/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetElementChoiceSets(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, elementChoiceSetUuid, callback)</td>
    <td style="padding:15px">returns element-choice-sets</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/element-choice-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSetRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, callback)</td>
    <td style="padding:15px">returns constrained-element-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing constrained-element-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, body, callback)</td>
    <td style="padding:15px">update an existing constrained-element-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSetsConstrainedElementSet(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing constrained-element-set</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets/constrained-element-set/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraintConstrainedElementSets(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, constrainedElementSetUuid, callback)</td>
    <td style="padding:15px">returns constrained-element-sets</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}/constrained-element-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraint(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, callback)</td>
    <td style="padding:15px">returns model-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraint(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing model-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraint(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, body, callback)</td>
    <td style="padding:15px">update an existing model-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraintsModelConstraint(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing model-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints/model-constraint/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementModelConstraints(modelInvariantId, modelVersionId, modelElementUuid, modelConstraintUuid, callback)</td>
    <td style="padding:15px">returns model-constraints</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/model-constraints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElementRelationshipListRelationship(modelInvariantId, modelVersionId, modelElementUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElement(modelInvariantId, modelVersionId, modelElementUuid, callback)</td>
    <td style="padding:15px">returns model-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElement(modelInvariantId, modelVersionId, modelElementUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing model-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElement(modelInvariantId, modelVersionId, modelElementUuid, body, callback)</td>
    <td style="padding:15px">update an existing model-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerModelElementsModelElement(modelInvariantId, modelVersionId, modelElementUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing model-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements/model-element/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerModelElements(modelInvariantId, modelVersionId, modelElementUuid, callback)</td>
    <td style="padding:15px">returns model-elements</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/model-elements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerMetadataMetadatum(modelInvariantId, modelVersionId, metaname, callback)</td>
    <td style="padding:15px">returns metadatum</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/metadata/metadatum/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerMetadataMetadatum(modelInvariantId, modelVersionId, metaname, body, callback)</td>
    <td style="padding:15px">create or update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/metadata/metadatum/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVerMetadataMetadatum(modelInvariantId, modelVersionId, metaname, body, callback)</td>
    <td style="padding:15px">update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/metadata/metadatum/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerMetadataMetadatum(modelInvariantId, modelVersionId, metaname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/metadata/metadatum/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVerMetadata(modelInvariantId, modelVersionId, metaname, callback)</td>
    <td style="padding:15px">returns metadata</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVerRelationshipListRelationship(modelInvariantId, modelVersionId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVerRelationshipListRelationship(modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVersModelVer(modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns model-ver</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelModelVersModelVer(modelInvariantId, modelVersionId, body, callback)</td>
    <td style="padding:15px">create or update an existing model-ver</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModelModelVersModelVer(modelInvariantId, modelVersionId, body, callback)</td>
    <td style="padding:15px">update an existing model-ver</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelModelVersModelVer(modelInvariantId, modelVersionId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing model-ver</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers/model-ver/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModelModelVers(modelInvariantId, modelVersionId, modelName, modelVersion, distributionStatus, callback)</td>
    <td style="padding:15px">returns model-vers</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/model-vers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModelRelationshipListRelationship(modelInvariantId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModelRelationshipListRelationship(modelInvariantId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModelsModel(modelInvariantId, callback)</td>
    <td style="padding:15px">returns model</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationModelsModel(modelInvariantId, body, callback)</td>
    <td style="padding:15px">create or update an existing model</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationModelsModel(modelInvariantId, body, callback)</td>
    <td style="padding:15px">update an existing model</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationModelsModel(modelInvariantId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing model</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models/model/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationModels(modelInvariantId, modelType, callback)</td>
    <td style="padding:15px">returns models</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookupRelationshipListRelationship(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookupRelationshipListRelationship(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookup(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, callback)</td>
    <td style="padding:15px">returns related-lookup</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookup(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing related-lookup</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookup(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, body, callback)</td>
    <td style="padding:15px">update an existing related-lookup</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookupsRelatedLookup(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing related-lookup</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups/related-lookup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelatedLookups(namedQueryUuid, namedQueryElementUuid, relatedLookupUuid, callback)</td>
    <td style="padding:15px">returns related-lookups</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/related-lookups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementPropertyConstraintsPropertyConstraint(namedQueryUuid, namedQueryElementUuid, propertyConstraintUuid, callback)</td>
    <td style="padding:15px">returns property-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/property-constraints/property-constraint/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementPropertyConstraintsPropertyConstraint(namedQueryUuid, namedQueryElementUuid, propertyConstraintUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing property-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/property-constraints/property-constraint/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementPropertyConstraintsPropertyConstraint(namedQueryUuid, namedQueryElementUuid, propertyConstraintUuid, body, callback)</td>
    <td style="padding:15px">update an existing property-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/property-constraints/property-constraint/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementPropertyConstraintsPropertyConstraint(namedQueryUuid, namedQueryElementUuid, propertyConstraintUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing property-constraint</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/property-constraints/property-constraint/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementPropertyConstraints(namedQueryUuid, namedQueryElementUuid, propertyConstraintUuid, callback)</td>
    <td style="padding:15px">returns property-constraints</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/property-constraints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelationshipListRelationship(namedQueryUuid, namedQueryElementUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElementRelationshipListRelationship(namedQueryUuid, namedQueryElementUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElement(namedQueryUuid, namedQueryElementUuid, callback)</td>
    <td style="padding:15px">returns named-query-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElement(namedQueryUuid, namedQueryElementUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing named-query-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElement(namedQueryUuid, namedQueryElementUuid, body, callback)</td>
    <td style="padding:15px">update an existing named-query-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElementsNamedQueryElement(namedQueryUuid, namedQueryElementUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing named-query-element</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements/named-query-element/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQueryNamedQueryElements(namedQueryUuid, namedQueryElementUuid, callback)</td>
    <td style="padding:15px">returns named-query-elements</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/named-query-elements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQueryRelationshipListRelationship(namedQueryUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQueryRelationshipListRelationship(namedQueryUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueriesNamedQuery(namedQueryUuid, callback)</td>
    <td style="padding:15px">returns named-query</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateServiceDesignAndCreationNamedQueriesNamedQuery(namedQueryUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing named-query</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceDesignAndCreationNamedQueriesNamedQuery(namedQueryUuid, body, callback)</td>
    <td style="padding:15px">update an existing named-query</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDesignAndCreationNamedQueriesNamedQuery(namedQueryUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing named-query</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries/named-query/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDesignAndCreationNamedQueries(namedQueryUuid, namedQueryName, callback)</td>
    <td style="padding:15px">returns named-queries</td>
    <td style="padding:15px">{base_path}/{version}/service-design-and-creation/named-queries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLogicalLinksLogicalLinkRelationshipListRelationship(linkName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLogicalLinksLogicalLinkRelationshipListRelationship(linkName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLogicalLinksLogicalLink(linkName, callback)</td>
    <td style="padding:15px">returns logical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLogicalLinksLogicalLink(linkName, body, callback)</td>
    <td style="padding:15px">create or update an existing logical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkLogicalLinksLogicalLink(linkName, body, callback)</td>
    <td style="padding:15px">update an existing logical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLogicalLinksLogicalLink(linkName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing logical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links/logical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLogicalLinks(linkName, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, operationalStatus, provStatus, linkId, circuitId, purpose, callback)</td>
    <td style="padding:15px">returns logical-links</td>
    <td style="padding:15px">{base_path}/{version}/network/logical-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfServiceRelationshipListRelationship(sitePairSetId, routingInstanceId, sitePairId, cos, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfServiceRelationshipListRelationship(sitePairSetId, routingInstanceId, sitePairId, cos, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfService(sitePairSetId, routingInstanceId, sitePairId, cos, callback)</td>
    <td style="padding:15px">returns class-of-service</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfService(sitePairSetId, routingInstanceId, sitePairId, cos, body, callback)</td>
    <td style="padding:15px">create or update an existing class-of-service</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfService(sitePairSetId, routingInstanceId, sitePairId, cos, body, callback)</td>
    <td style="padding:15px">update an existing class-of-service</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfServiceClassOfService(sitePairSetId, routingInstanceId, sitePairId, cos, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing class-of-service</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service/class-of-service/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairClassesOfService(sitePairSetId, routingInstanceId, sitePairId, cos, callback)</td>
    <td style="padding:15px">returns classes-of-service</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/classes-of-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairRelationshipListRelationship(sitePairSetId, routingInstanceId, sitePairId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePairRelationshipListRelationship(sitePairSetId, routingInstanceId, sitePairId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePair(sitePairSetId, routingInstanceId, sitePairId, callback)</td>
    <td style="padding:15px">returns site-pair</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePair(sitePairSetId, routingInstanceId, sitePairId, body, callback)</td>
    <td style="padding:15px">create or update an existing site-pair</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePair(sitePairSetId, routingInstanceId, sitePairId, body, callback)</td>
    <td style="padding:15px">update an existing site-pair</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairsSitePair(sitePairSetId, routingInstanceId, sitePairId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing site-pair</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs/site-pair/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceSitePairs(sitePairSetId, routingInstanceId, sitePairId, callback)</td>
    <td style="padding:15px">returns site-pairs</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/site-pairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceRelationshipListRelationship(sitePairSetId, routingInstanceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstanceRelationshipListRelationship(sitePairSetId, routingInstanceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstance(sitePairSetId, routingInstanceId, callback)</td>
    <td style="padding:15px">returns routing-instance</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstance(sitePairSetId, routingInstanceId, body, callback)</td>
    <td style="padding:15px">create or update an existing routing-instance</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstance(sitePairSetId, routingInstanceId, body, callback)</td>
    <td style="padding:15px">update an existing routing-instance</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRoutingInstancesRoutingInstance(sitePairSetId, routingInstanceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing routing-instance</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances/routing-instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSetRoutingInstances(sitePairSetId, routingInstanceId, callback)</td>
    <td style="padding:15px">returns routing-instances</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/routing-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSetRelationshipListRelationship(sitePairSetId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSetRelationshipListRelationship(sitePairSetId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSetsSitePairSet(sitePairSetId, callback)</td>
    <td style="padding:15px">returns site-pair-set</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSitePairSetsSitePairSet(sitePairSetId, body, callback)</td>
    <td style="padding:15px">create or update an existing site-pair-set</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSitePairSetsSitePairSet(sitePairSetId, body, callback)</td>
    <td style="padding:15px">update an existing site-pair-set</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSitePairSetsSitePairSet(sitePairSetId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing site-pair-set</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets/site-pair-set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSitePairSets(sitePairSetId, callback)</td>
    <td style="padding:15px">returns site-pair-sets</td>
    <td style="padding:15px">{base_path}/{version}/network/site-pair-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVpnBindingsVpnBindingRouteTargetsRouteTargetRelationshipListRelationship(vpnId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVpnBindingsVpnBindingRouteTargetsRouteTargetRelationshipListRelationship(vpnId, globalRouteTarget, routeTargetRole, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVpnBindingsVpnBindingRouteTargetsRouteTarget(vpnId, globalRouteTarget, routeTargetRole, callback)</td>
    <td style="padding:15px">returns route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVpnBindingsVpnBindingRouteTargetsRouteTarget(vpnId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">create or update an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVpnBindingsVpnBindingRouteTargetsRouteTarget(vpnId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">update an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVpnBindingsVpnBindingRouteTargetsRouteTarget(vpnId, globalRouteTarget, routeTargetRole, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets/route-target/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVpnBindingsVpnBindingRouteTargets(vpnId, callback)</td>
    <td style="padding:15px">returns route-targets</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/route-targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVpnBindingsVpnBindingRelationshipListRelationship(vpnId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVpnBindingsVpnBindingRelationshipListRelationship(vpnId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVpnBindingsVpnBinding(vpnId, callback)</td>
    <td style="padding:15px">returns vpn-binding</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVpnBindingsVpnBinding(vpnId, body, callback)</td>
    <td style="padding:15px">create or update an existing vpn-binding</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVpnBindingsVpnBinding(vpnId, body, callback)</td>
    <td style="padding:15px">update an existing vpn-binding</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVpnBindingsVpnBinding(vpnId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vpn-binding</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings/vpn-binding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVpnBindings(vpnId, vpnName, vpnType, accessProviderId, accessClientId, accessTopologyId, srcAccessNodeId, srcAccessLtpId, dstAccessNodeId, dstAccessLtpId, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns vpn-bindings</td>
    <td style="padding:15px">{base_path}/{version}/network/vpn-bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeRelationshipListRelationship(equipmentName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeRelationshipListRelationship(equipmentName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceRelationshipListRelationship(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceRelationshipListRelationship(equipmentName, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(equipmentName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(equipmentName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPf(equipmentName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPf(equipmentName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPf(equipmentName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfsSriovPf(equipmentName, interfaceName, pfPciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceSriovPfs(equipmentName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pfs</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceVlans(equipmentName, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceSriovVfs(equipmentName, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterfaceLInterfaces(equipmentName, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfacesPInterface(equipmentName, interfaceName, callback)</td>
    <td style="padding:15px">returns p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPePInterfacesPInterface(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPePInterfacesPInterface(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPePInterfacesPInterface(equipmentName, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPePInterfaces(equipmentName, interfaceName, provStatus, operationalStatus, callback)</td>
    <td style="padding:15px">returns p-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/p-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceRelationshipListRelationship(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceRelationshipListRelationship(equipmentName, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(equipmentName, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceVlans(equipmentName, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(equipmentName, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfs(equipmentName, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(equipmentName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfacesLInterface(equipmentName, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterfaceLInterfaces(equipmentName, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfacesLagInterface(equipmentName, interfaceName, callback)</td>
    <td style="padding:15px">returns lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPeLagInterfacesLagInterface(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPeLagInterfacesLagInterface(equipmentName, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPeLagInterfacesLagInterface(equipmentName, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPeLagInterfaces(equipmentName, interfaceName, interfaceId, interfaceRole, callback)</td>
    <td style="padding:15px">returns lag-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}/lag-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPesVplsPe(equipmentName, callback)</td>
    <td style="padding:15px">returns vpls-pe</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVplsPesVplsPe(equipmentName, body, callback)</td>
    <td style="padding:15px">create or update an existing vpls-pe</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVplsPesVplsPe(equipmentName, body, callback)</td>
    <td style="padding:15px">update an existing vpls-pe</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVplsPesVplsPe(equipmentName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vpls-pe</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes/vpls-pe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVplsPes(equipmentName, provStatus, callback)</td>
    <td style="padding:15px">returns vpls-pes</td>
    <td style="padding:15px">{base_path}/{version}/network/vpls-pes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkMulticastConfigurationsMulticastConfigurationRelationshipListRelationship(multicastConfigurationId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkMulticastConfigurationsMulticastConfigurationRelationshipListRelationship(multicastConfigurationId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkMulticastConfigurationsMulticastConfiguration(multicastConfigurationId, callback)</td>
    <td style="padding:15px">returns multicast-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkMulticastConfigurationsMulticastConfiguration(multicastConfigurationId, body, callback)</td>
    <td style="padding:15px">create or update an existing multicast-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkMulticastConfigurationsMulticastConfiguration(multicastConfigurationId, body, callback)</td>
    <td style="padding:15px">update an existing multicast-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkMulticastConfigurationsMulticastConfiguration(multicastConfigurationId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing multicast-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations/multicast-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkMulticastConfigurations(multicastConfigurationId, callback)</td>
    <td style="padding:15px">returns multicast-configurations</td>
    <td style="padding:15px">{base_path}/{version}/network/multicast-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntryRelationshipListRelationship(vnfId, interfaceId, cvlanTag, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntryRelationshipListRelationship(vnfId, interfaceId, cvlanTag, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntry(vnfId, interfaceId, cvlanTag, callback)</td>
    <td style="padding:15px">returns cvlan-tag-entry</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntry(vnfId, interfaceId, cvlanTag, body, callback)</td>
    <td style="padding:15px">create or update an existing cvlan-tag-entry</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntry(vnfId, interfaceId, cvlanTag, body, callback)</td>
    <td style="padding:15px">update an existing cvlan-tag-entry</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVcePortGroupsPortGroupCvlanTagsCvlanTagEntry(vnfId, interfaceId, cvlanTag, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing cvlan-tag-entry</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags/cvlan-tag-entry/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVcePortGroupsPortGroupCvlanTags(vnfId, interfaceId, cvlanTag, callback)</td>
    <td style="padding:15px">returns cvlan-tags</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/cvlan-tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVcePortGroupsPortGroupRelationshipListRelationship(vnfId, interfaceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVcePortGroupsPortGroupRelationshipListRelationship(vnfId, interfaceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVcePortGroupsPortGroup(vnfId, interfaceId, callback)</td>
    <td style="padding:15px">returns port-group</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVcePortGroupsPortGroup(vnfId, interfaceId, body, callback)</td>
    <td style="padding:15px">create or update an existing port-group</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVcesVcePortGroupsPortGroup(vnfId, interfaceId, body, callback)</td>
    <td style="padding:15px">update an existing port-group</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVcePortGroupsPortGroup(vnfId, interfaceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing port-group</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups/port-group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVcePortGroups(vnfId, interfaceId, portGroupId, switchName, heatStackId, callback)</td>
    <td style="padding:15px">returns port-groups</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/port-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVceLicensesLicenseRelationshipListRelationship(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVceLicensesLicenseRelationshipListRelationship(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVceLicensesLicense(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns license</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVceLicensesLicense(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVcesVceLicensesLicense(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">update an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVceLicensesLicense(vnfId, groupUuid, resourceUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVceLicenses(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns licenses</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVceEntitlementsEntitlementRelationshipListRelationship(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVceEntitlementsEntitlementRelationshipListRelationship(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVceEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVceEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVcesVceEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">update an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVceEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVceEntitlements(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns entitlements</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/entitlements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVceRelationshipListRelationship(vnfId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVceRelationshipListRelationship(vnfId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVcesVce(vnfId, callback)</td>
    <td style="padding:15px">returns vce</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVcesVce(vnfId, body, callback)</td>
    <td style="padding:15px">create or update an existing vce</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVcesVce(vnfId, body, callback)</td>
    <td style="padding:15px">update an existing vce</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVcesVce(vnfId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vce</td>
    <td style="padding:15px">{base_path}/{version}/network/vces/vce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVces(vnfId, vnfName, vnfName2, vnfType, serviceId, regionalResourceZone, provStatus, heatStackId, vpeId, callback)</td>
    <td style="padding:15px">returns vces</td>
    <td style="padding:15px">{base_path}/{version}/network/vces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcRelationshipListRelationship(vnfcName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcRelationshipListRelationship(vnfcName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCpRelationshipListRelationship(vnfcName, cpInstanceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCpRelationshipListRelationship(vnfcName, cpInstanceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressListRelationshipListRelationship(vnfcName, cpInstanceId, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressListRelationshipListRelationship(vnfcName, cpInstanceId, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressList(vnfcName, cpInstanceId, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressList(vnfcName, cpInstanceId, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressList(vnfcName, cpInstanceId, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCpL3InterfaceIpv4AddressList(vnfcName, cpInstanceId, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressListRelationshipListRelationship(vnfcName, cpInstanceId, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressListRelationshipListRelationship(vnfcName, cpInstanceId, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressList(vnfcName, cpInstanceId, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressList(vnfcName, cpInstanceId, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressList(vnfcName, cpInstanceId, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCpL3InterfaceIpv6AddressList(vnfcName, cpInstanceId, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcCpsCp(vnfcName, cpInstanceId, callback)</td>
    <td style="padding:15px">returns cp</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcCpsCp(vnfcName, cpInstanceId, body, callback)</td>
    <td style="padding:15px">create or update an existing cp</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfcCpsCp(vnfcName, cpInstanceId, body, callback)</td>
    <td style="padding:15px">update an existing cp</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcCpsCp(vnfcName, cpInstanceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing cp</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps/cp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcCps(vnfcName, callback)</td>
    <td style="padding:15px">returns cps</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/cps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcL3InterfaceIpv4AddressListRelationshipListRelationship(vnfcName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcL3InterfaceIpv4AddressListRelationshipListRelationship(vnfcName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcL3InterfaceIpv4AddressList(vnfcName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcL3InterfaceIpv4AddressList(vnfcName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfcL3InterfaceIpv4AddressList(vnfcName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcL3InterfaceIpv4AddressList(vnfcName, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv4-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcL3InterfaceIpv6AddressListRelationshipListRelationship(vnfcName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcL3InterfaceIpv6AddressListRelationshipListRelationship(vnfcName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfcL3InterfaceIpv6AddressList(vnfcName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfcL3InterfaceIpv6AddressList(vnfcName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfcL3InterfaceIpv6AddressList(vnfcName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfcL3InterfaceIpv6AddressList(vnfcName, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}/l3-interface-ipv6-address-list/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcsVnfc(vnfcName, callback)</td>
    <td style="padding:15px">returns vnfc</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVnfcsVnfc(vnfcName, body, callback)</td>
    <td style="padding:15px">create or update an existing vnfc</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVnfcsVnfc(vnfcName, body, callback)</td>
    <td style="padding:15px">update an existing vnfc</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVnfcsVnfc(vnfcName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vnfc</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs/vnfc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVnfcs(vnfcName, nfcNamingCode, nfcFunction, provStatus, ipaddressV4OamVip, inMaint, isClosedLoopDisabled, groupNotation, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns vnfcs</td>
    <td style="padding:15px">{base_path}/{version}/network/vnfcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRouteRelationshipListRelationship(networkId, subnetId, hostRouteId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRouteRelationshipListRelationship(networkId, subnetId, hostRouteId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRoute(networkId, subnetId, hostRouteId, callback)</td>
    <td style="padding:15px">returns host-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRoute(networkId, subnetId, hostRouteId, body, callback)</td>
    <td style="padding:15px">create or update an existing host-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRoute(networkId, subnetId, hostRouteId, body, callback)</td>
    <td style="padding:15px">update an existing host-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutesHostRoute(networkId, subnetId, hostRouteId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing host-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes/host-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSubnetsSubnetHostRoutes(networkId, subnetId, hostRouteId, callback)</td>
    <td style="padding:15px">returns host-routes</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/host-routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSubnetsSubnetRelationshipListRelationship(networkId, subnetId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSubnetsSubnetRelationshipListRelationship(networkId, subnetId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSubnetsSubnet(networkId, subnetId, callback)</td>
    <td style="padding:15px">returns subnet</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSubnetsSubnet(networkId, subnetId, body, callback)</td>
    <td style="padding:15px">create or update an existing subnet</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3NetworkSubnetsSubnet(networkId, subnetId, body, callback)</td>
    <td style="padding:15px">update an existing subnet</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSubnetsSubnet(networkId, subnetId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing subnet</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets/subnet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSubnets(networkId, subnetId, subnetName, callback)</td>
    <td style="padding:15px">returns subnets</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignmentRelationshipListRelationship(networkId, vlanIdInner, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignmentRelationshipListRelationship(networkId, vlanIdInner, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignment(networkId, vlanIdInner, callback)</td>
    <td style="padding:15px">returns ctag-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignment(networkId, vlanIdInner, body, callback)</td>
    <td style="padding:15px">create or update an existing ctag-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignment(networkId, vlanIdInner, body, callback)</td>
    <td style="padding:15px">update an existing ctag-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkCtagAssignmentsCtagAssignment(networkId, vlanIdInner, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing ctag-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments/ctag-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkCtagAssignments(networkId, vlanIdInner, callback)</td>
    <td style="padding:15px">returns ctag-assignments</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/ctag-assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignmentRelationshipListRelationship(networkId, segmentationId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignmentRelationshipListRelationship(networkId, segmentationId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignment(networkId, segmentationId, callback)</td>
    <td style="padding:15px">returns segmentation-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignment(networkId, segmentationId, body, callback)</td>
    <td style="padding:15px">create or update an existing segmentation-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignment(networkId, segmentationId, body, callback)</td>
    <td style="padding:15px">update an existing segmentation-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkSegmentationAssignmentsSegmentationAssignment(networkId, segmentationId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing segmentation-assignment</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments/segmentation-assignment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkSegmentationAssignments(networkId, segmentationId, callback)</td>
    <td style="padding:15px">returns segmentation-assignments</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/segmentation-assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkAggregateRoutesAggregateRouteRelationshipListRelationship(networkId, routeId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkAggregateRoutesAggregateRouteRelationshipListRelationship(networkId, routeId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkAggregateRoutesAggregateRoute(networkId, routeId, callback)</td>
    <td style="padding:15px">returns aggregate-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkAggregateRoutesAggregateRoute(networkId, routeId, body, callback)</td>
    <td style="padding:15px">create or update an existing aggregate-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3NetworkAggregateRoutesAggregateRoute(networkId, routeId, body, callback)</td>
    <td style="padding:15px">update an existing aggregate-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkAggregateRoutesAggregateRoute(networkId, routeId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing aggregate-route</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes/aggregate-route/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3NetworkAggregateRoutes(networkId, callback)</td>
    <td style="padding:15px">returns aggregate-routes</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/aggregate-routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3NetworkRelationshipListRelationship(networkId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3NetworkRelationshipListRelationship(networkId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3NetworksL3Network(networkId, callback)</td>
    <td style="padding:15px">returns l3-network</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkL3NetworksL3Network(networkId, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-network</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3NetworksL3Network(networkId, body, callback)</td>
    <td style="padding:15px">update an existing l3-network</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkL3NetworksL3Network(networkId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-network</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks/l3-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3Networks(networkId, networkName, networkRole, serviceId, heatStackId, contrailNetworkFqdn, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, callback)</td>
    <td style="padding:15px">returns l3-networks</td>
    <td style="padding:15px">{base_path}/{version}/network/l3-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNetworkPoliciesNetworkPolicyRelationshipListRelationship(networkPolicyId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNetworkPoliciesNetworkPolicyRelationshipListRelationship(networkPolicyId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetworkPoliciesNetworkPolicy(networkPolicyId, callback)</td>
    <td style="padding:15px">returns network-policy</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNetworkPoliciesNetworkPolicy(networkPolicyId, body, callback)</td>
    <td style="padding:15px">create or update an existing network-policy</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNetworkPoliciesNetworkPolicy(networkPolicyId, body, callback)</td>
    <td style="padding:15px">update an existing network-policy</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNetworkPoliciesNetworkPolicy(networkPolicyId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing network-policy</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies/network-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetworkPolicies(networkPolicyId, networkPolicyFqdn, callback)</td>
    <td style="padding:15px">returns network-policies</td>
    <td style="padding:15px">{base_path}/{version}/network/network-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfRelationshipListRelationship(vnfId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfRelationshipListRelationship(vnfId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceVlans(vnfId, interfaceName, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId, interfaceName, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceSriovVfs(vnfId, interfaceName, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceRelationshipListRelationship(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceRelationshipListRelationship(vnfId, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfacesLInterface(vnfId, interfaceName, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLInterfacesLInterface(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLInterfacesLInterface(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLInterfacesLInterface(vnfId, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLInterfaces(vnfId, interfaceName, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceRelationshipListRelationship(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceRelationshipListRelationship(vnfId, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(vnfId, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlans(vnfId, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(vnfId, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfs(vnfId, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterface(vnfId, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterface(vnfId, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterface(vnfId, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfacesLInterface(vnfId, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterfaceLInterfaces(vnfId, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfacesLagInterface(vnfId, interfaceName, callback)</td>
    <td style="padding:15px">returns lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLagInterfacesLagInterface(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLagInterfacesLagInterface(vnfId, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLagInterfacesLagInterface(vnfId, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLagInterfaces(vnfId, interfaceName, interfaceId, interfaceRole, callback)</td>
    <td style="padding:15px">returns lag-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/lag-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfVfModulesVfModuleRelationshipListRelationship(vnfId, vfModuleId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfVfModulesVfModuleRelationshipListRelationship(vnfId, vfModuleId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfVfModulesVfModule(vnfId, vfModuleId, callback)</td>
    <td style="padding:15px">returns vf-module</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfVfModulesVfModule(vnfId, vfModuleId, body, callback)</td>
    <td style="padding:15px">create or update an existing vf-module</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfVfModulesVfModule(vnfId, vfModuleId, body, callback)</td>
    <td style="padding:15px">update an existing vf-module</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfVfModulesVfModule(vnfId, vfModuleId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vf-module</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules/vf-module/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfVfModules(vnfId, vfModuleId, vfModuleName, heatStackId, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, contrailServiceInstanceFqdn, callback)</td>
    <td style="padding:15px">returns vf-modules</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/vf-modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLicensesLicenseRelationshipListRelationship(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLicensesLicenseRelationshipListRelationship(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLicensesLicense(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns license</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfLicensesLicense(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfLicensesLicense(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">update an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfLicensesLicense(vnfId, groupUuid, resourceUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing license</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses/license/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfLicenses(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns licenses</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfEntitlementsEntitlementRelationshipListRelationship(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfEntitlementsEntitlementRelationshipListRelationship(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnfEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">create or update an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnfEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, body, callback)</td>
    <td style="padding:15px">update an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnfEntitlementsEntitlement(vnfId, groupUuid, resourceUuid, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing entitlement</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements/entitlement/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnfEntitlements(vnfId, groupUuid, resourceUuid, callback)</td>
    <td style="padding:15px">returns entitlements</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}/entitlements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfsGenericVnf(vnfId, callback)</td>
    <td style="padding:15px">returns generic-vnf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkGenericVnfsGenericVnf(vnfId, body, callback)</td>
    <td style="padding:15px">create or update an existing generic-vnf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGenericVnfsGenericVnf(vnfId, body, callback)</td>
    <td style="padding:15px">update an existing generic-vnf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGenericVnfsGenericVnf(vnfId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing generic-vnf</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs/generic-vnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGenericVnfs(vnfId, vnfName, vnfName2, vnfType, serviceId, regionalResourceZone, provStatus, heatStackId, inMaint, isClosedLoopDisabled, modelInvariantId, modelVersionId, widgetModelId, widgetModelVersion, nfType, nfFunction, nfRole, nfNamingCode, callback)</td>
    <td style="padding:15px">returns generic-vnfs</td>
    <td style="padding:15px">{base_path}/{version}/network/generic-vnfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLagLinksLagLinkRelationshipListRelationship(linkName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLagLinksLagLinkRelationshipListRelationship(linkName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLagLinksLagLink(linkName, callback)</td>
    <td style="padding:15px">returns lag-link</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLagLinksLagLink(linkName, body, callback)</td>
    <td style="padding:15px">create or update an existing lag-link</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkLagLinksLagLink(linkName, body, callback)</td>
    <td style="padding:15px">update an existing lag-link</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLagLinksLagLink(linkName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lag-link</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links/lag-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLagLinks(linkName, callback)</td>
    <td style="padding:15px">returns lag-links</td>
    <td style="padding:15px">{base_path}/{version}/network/lag-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceRelationshipListRelationship(vnfId2, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceRelationshipListRelationship(vnfId2, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(vnfId2, interfaceName, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlan(vnfId2, interfaceName, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlan(vnfId2, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlan(vnfId2, interfaceName, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceVlansVlan(vnfId2, interfaceName, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans/vlan/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceVlans(vnfId2, interfaceName, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId2, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(vnfId2, interfaceName, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVf(vnfId2, interfaceName, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVf(vnfId2, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVf(vnfId2, interfaceName, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfsSriovVf(vnfId2, interfaceName, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs/sriov-vf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceSriovVfs(vnfId2, interfaceName, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceRelationshipListRelationship(vnfId2, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceRelationshipListRelationship(vnfId2, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId2, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(vnfId2, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId2, interfaceName, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId2, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId2, interfaceName, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv4AddressList(vnfId2, interfaceName, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv4-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId2, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(vnfId2, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId2, interfaceName, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId2, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId2, interfaceName, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterfaceL3InterfaceIpv6AddressList(vnfId2, interfaceName, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}/l3-interface-ipv6-address-list/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfacesLInterface(vnfId2, interfaceName, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvceLInterfacesLInterface(vnfId2, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvceLInterfacesLInterface(vnfId2, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvceLInterfacesLInterface(vnfId2, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces/l-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvceLInterfaces(vnfId2, interfaceName, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvcesNewvce(vnfId2, callback)</td>
    <td style="padding:15px">returns newvce</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNewvcesNewvce(vnfId2, body, callback)</td>
    <td style="padding:15px">create or update an existing newvce</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNewvcesNewvce(vnfId2, body, callback)</td>
    <td style="padding:15px">update an existing newvce</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNewvcesNewvce(vnfId2, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing newvce</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces/newvce/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNewvces(vnfId2, vnfName, vnfName2, vnfType, provStatus, heatStackId, callback)</td>
    <td style="padding:15px">returns newvces</td>
    <td style="padding:15px">{base_path}/{version}/network/newvces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfSoftwareVersionsSoftwareVersion(pnfName, softwareVersionId, callback)</td>
    <td style="padding:15px">returns software-version</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/software-versions/software-version/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfSoftwareVersionsSoftwareVersion(pnfName, softwareVersionId, body, callback)</td>
    <td style="padding:15px">create or update an existing software-version</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/software-versions/software-version/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfSoftwareVersionsSoftwareVersion(pnfName, softwareVersionId, body, callback)</td>
    <td style="padding:15px">update an existing software-version</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/software-versions/software-version/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfSoftwareVersionsSoftwareVersion(pnfName, softwareVersionId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing software-version</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/software-versions/software-version/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfSoftwareVersions(pnfName, callback)</td>
    <td style="padding:15px">returns software-versions</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/software-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfRelationshipListRelationship(pnfName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfRelationshipListRelationship(pnfName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceRelationshipListRelationship(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceRelationshipListRelationship(pnfName, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(pnfName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPfRelationshipListRelationship(pnfName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPf(pnfName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPf(pnfName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPf(pnfName, interfaceName, pfPciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceSriovPfsSriovPf(pnfName, interfaceName, pfPciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-pf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs/sriov-pf/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceSriovPfs(pnfName, interfaceName, pfPciId, callback)</td>
    <td style="padding:15px">returns sriov-pfs</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/sriov-pfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceVlans(pnfName, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceSriovVfs(pnfName, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterfaceLInterfaces(pnfName, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfacesPInterface(pnfName, interfaceName, callback)</td>
    <td style="padding:15px">returns p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfPInterfacesPInterface(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfPInterfacesPInterface(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfPInterfacesPInterface(pnfName, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing p-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces/p-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfPInterfaces(pnfName, interfaceName, provStatus, operationalStatus, callback)</td>
    <td style="padding:15px">returns p-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/p-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceRelationshipListRelationship(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceRelationshipListRelationship(pnfName, interfaceName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv4-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlanL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, vlanInterface, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}/l3-interface-ipv6-address-list/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, callback)</td>
    <td style="padding:15px">returns vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, body, callback)</td>
    <td style="padding:15px">update an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlansVlan(pnfName, interfaceName, interfaceNamePath, vlanInterface, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans/vlan/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceVlans(pnfName, interfaceName, interfaceNamePath, vlanInterface, vlanIdInner, vpnKey, callback)</td>
    <td style="padding:15px">returns vlans</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVfRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, callback)</td>
    <td style="padding:15px">returns sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">create or update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, body, callback)</td>
    <td style="padding:15px">update an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfsSriovVf(pnfName, interfaceName, interfaceNamePath, pciId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sriov-vf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs/sriov-vf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceSriovVfs(pnfName, interfaceName, interfaceNamePath, pciId, vfVlanFilter, vfMacFilter, vfVlanStrip, neutronNetworkId, callback)</td>
    <td style="padding:15px">returns sriov-vfs</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/sriov-vfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv4AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv4Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv4-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv4-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressListRelationshipListRelationship(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, callback)</td>
    <td style="padding:15px">returns l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">create or update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, body, callback)</td>
    <td style="padding:15px">update an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterfaceL3InterfaceIpv6AddressList(pnfName, interfaceName, interfaceNamePath, l3InterfaceIpv6Address, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l3-interface-ipv6-address-list</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}/l3-interface-ipv6-address-list/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, callback)</td>
    <td style="padding:15px">returns l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">create or update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, body, callback)</td>
    <td style="padding:15px">update an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterfaceLInterfacesLInterface(pnfName, interfaceName, interfaceNamePath, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing l-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces/l-interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterfaceLInterfaces(pnfName, interfaceName, interfaceNameQuery, interfaceId, macaddr, networkName, callback)</td>
    <td style="padding:15px">returns l-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}/l-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfacesLagInterface(pnfName, interfaceName, callback)</td>
    <td style="padding:15px">returns lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfLagInterfacesLagInterface(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">create or update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfLagInterfacesLagInterface(pnfName, interfaceName, body, callback)</td>
    <td style="padding:15px">update an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfLagInterfacesLagInterface(pnfName, interfaceName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lag-interface</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces/lag-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfLagInterfaces(pnfName, interfaceName, interfaceId, interfaceRole, callback)</td>
    <td style="padding:15px">returns lag-interfaces</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/lag-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfVrfsVrfRouteTargetsRouteTargetRelationshipListRelationship(pnfName, vrfId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfVrfsVrfRouteTargetsRouteTargetRelationshipListRelationship(pnfName, vrfId, globalRouteTarget, routeTargetRole, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfVrfsVrfRouteTargetsRouteTarget(pnfName, vrfId, globalRouteTarget, routeTargetRole, callback)</td>
    <td style="padding:15px">returns route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfVrfsVrfRouteTargetsRouteTarget(pnfName, vrfId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">create or update an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfVrfsVrfRouteTargetsRouteTarget(pnfName, vrfId, globalRouteTarget, routeTargetRole, body, callback)</td>
    <td style="padding:15px">update an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfVrfsVrfRouteTargetsRouteTarget(pnfName, vrfId, globalRouteTarget, routeTargetRole, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing route-target</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets/route-target/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfVrfsVrfRouteTargets(pnfName, vrfId, callback)</td>
    <td style="padding:15px">returns route-targets</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/route-targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfVrfsVrfRelationshipListRelationship(pnfName, vrfId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfVrfsVrfRelationshipListRelationship(pnfName, vrfId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfVrfsVrf(pnfName, vrfId, callback)</td>
    <td style="padding:15px">returns vrf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnfVrfsVrf(pnfName, vrfId, body, callback)</td>
    <td style="padding:15px">create or update an existing vrf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnfVrfsVrf(pnfName, vrfId, body, callback)</td>
    <td style="padding:15px">update an existing vrf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnfVrfsVrf(pnfName, vrfId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vrf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs/vrf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnfVrfs(pnfName, vrfId, callback)</td>
    <td style="padding:15px">returns vrfs</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfsPnf(pnfName, callback)</td>
    <td style="padding:15px">returns pnf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPnfsPnf(pnfName, body, callback)</td>
    <td style="padding:15px">create or update an existing pnf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPnfsPnf(pnfName, body, callback)</td>
    <td style="padding:15px">update an existing pnf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPnfsPnf(pnfName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing pnf</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs/pnf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPnfs(pnfName, orchestrationStatus, invStatus, adminStatus, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns pnfs</td>
    <td style="padding:15px">{base_path}/{version}/network/pnfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPhysicalLinksPhysicalLinkRelationshipListRelationship(linkName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPhysicalLinksPhysicalLinkRelationshipListRelationship(linkName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPhysicalLinksPhysicalLink(linkName, callback)</td>
    <td style="padding:15px">returns physical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkPhysicalLinksPhysicalLink(linkName, body, callback)</td>
    <td style="padding:15px">create or update an existing physical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPhysicalLinksPhysicalLink(linkName, body, callback)</td>
    <td style="padding:15px">update an existing physical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPhysicalLinksPhysicalLink(linkName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing physical-link</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links/physical-link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPhysicalLinks(linkName, circuitId, callback)</td>
    <td style="padding:15px">returns physical-links</td>
    <td style="padding:15px">{base_path}/{version}/network/physical-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkIpsecConfigurationsIpsecConfigurationRelationshipListRelationship(ipsecConfigurationId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkIpsecConfigurationsIpsecConfigurationRelationshipListRelationship(ipsecConfigurationId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServerRelationshipListRelationship(ipsecConfigurationId, vigAddressType, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServerRelationshipListRelationship(ipsecConfigurationId, vigAddressType, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServer(ipsecConfigurationId, vigAddressType, callback)</td>
    <td style="padding:15px">returns vig-server</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServer(ipsecConfigurationId, vigAddressType, body, callback)</td>
    <td style="padding:15px">create or update an existing vig-server</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServer(ipsecConfigurationId, vigAddressType, body, callback)</td>
    <td style="padding:15px">update an existing vig-server</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkIpsecConfigurationsIpsecConfigurationVigServersVigServer(ipsecConfigurationId, vigAddressType, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vig-server</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers/vig-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkIpsecConfigurationsIpsecConfigurationVigServers(ipsecConfigurationId, vigAddressType, callback)</td>
    <td style="padding:15px">returns vig-servers</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}/vig-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkIpsecConfigurationsIpsecConfiguration(ipsecConfigurationId, callback)</td>
    <td style="padding:15px">returns ipsec-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkIpsecConfigurationsIpsecConfiguration(ipsecConfigurationId, body, callback)</td>
    <td style="padding:15px">create or update an existing ipsec-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkIpsecConfigurationsIpsecConfiguration(ipsecConfigurationId, body, callback)</td>
    <td style="padding:15px">update an existing ipsec-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkIpsecConfigurationsIpsecConfiguration(ipsecConfigurationId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing ipsec-configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations/ipsec-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkIpsecConfigurations(ipsecConfigurationId, callback)</td>
    <td style="padding:15px">returns ipsec-configurations</td>
    <td style="padding:15px">{base_path}/{version}/network/ipsec-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkRouteTableReferencesRouteTableReferenceRelationshipListRelationship(routeTableReferenceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkRouteTableReferencesRouteTableReferenceRelationshipListRelationship(routeTableReferenceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkRouteTableReferencesRouteTableReference(routeTableReferenceId, callback)</td>
    <td style="padding:15px">returns route-table-reference</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkRouteTableReferencesRouteTableReference(routeTableReferenceId, body, callback)</td>
    <td style="padding:15px">create or update an existing route-table-reference</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkRouteTableReferencesRouteTableReference(routeTableReferenceId, body, callback)</td>
    <td style="padding:15px">update an existing route-table-reference</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkRouteTableReferencesRouteTableReference(routeTableReferenceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing route-table-reference</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references/route-table-reference/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkRouteTableReferences(routeTableReferenceId, routeTableReferenceFqdn, callback)</td>
    <td style="padding:15px">returns route-table-references</td>
    <td style="padding:15px">{base_path}/{version}/network/route-table-references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkInstanceGroupsInstanceGroupRelationshipListRelationship(id, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkInstanceGroupsInstanceGroupRelationshipListRelationship(id, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkInstanceGroupsInstanceGroup(id, callback)</td>
    <td style="padding:15px">returns instance-group</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkInstanceGroupsInstanceGroup(id, body, callback)</td>
    <td style="padding:15px">create or update an existing instance-group</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkInstanceGroupsInstanceGroup(id, body, callback)</td>
    <td style="padding:15px">update an existing instance-group</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkInstanceGroupsInstanceGroup(id, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing instance-group</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups/instance-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkInstanceGroups(id, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns instance-groups</td>
    <td style="padding:15px">{base_path}/{version}/network/instance-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkZonesZoneRelationshipListRelationship(zoneId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkZonesZoneRelationshipListRelationship(zoneId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkZonesZone(zoneId, callback)</td>
    <td style="padding:15px">returns zone</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkZonesZone(zoneId, body, callback)</td>
    <td style="padding:15px">create or update an existing zone</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkZonesZone(zoneId, body, callback)</td>
    <td style="padding:15px">update an existing zone</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkZonesZone(zoneId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing zone</td>
    <td style="padding:15px">{base_path}/{version}/network/zones/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkZones(zoneId, designType, zoneContext, callback)</td>
    <td style="padding:15px">returns zones</td>
    <td style="padding:15px">{base_path}/{version}/network/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationRelationshipListRelationship(configurationId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationRelationshipListRelationship(configurationId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationMetadataMetadatum(configurationId, metaname, callback)</td>
    <td style="padding:15px">returns metadatum</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationMetadataMetadatum(configurationId, metaname, body, callback)</td>
    <td style="padding:15px">create or update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConfigurationsConfigurationMetadataMetadatum(configurationId, metaname, body, callback)</td>
    <td style="padding:15px">update an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationMetadataMetadatum(configurationId, metaname, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing metadatum</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/metadata/metadatum/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationMetadata(configurationId, metaname, callback)</td>
    <td style="padding:15px">returns metadata</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMappingRelationshipListRelationship(configurationId, forwarderEvcId, vlanMappingId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMappingRelationshipListRelationship(configurationId, forwarderEvcId, vlanMappingId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMapping(configurationId, forwarderEvcId, vlanMappingId, callback)</td>
    <td style="padding:15px">returns vlan-mapping</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMapping(configurationId, forwarderEvcId, vlanMappingId, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan-mapping</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMapping(configurationId, forwarderEvcId, vlanMappingId, body, callback)</td>
    <td style="padding:15px">update an existing vlan-mapping</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappingsVlanMapping(configurationId, forwarderEvcId, vlanMappingId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan-mapping</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings/vlan-mapping/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcVlanMappings(configurationId, forwarderEvcId, callback)</td>
    <td style="padding:15px">returns vlan-mappings</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/vlan-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcRelationshipListRelationship(configurationId, forwarderEvcId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationForwarderEvcsForwarderEvcRelationshipListRelationship(configurationId, forwarderEvcId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationForwarderEvcsForwarderEvc(configurationId, forwarderEvcId, callback)</td>
    <td style="padding:15px">returns forwarder-evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvc(configurationId, forwarderEvcId, body, callback)</td>
    <td style="padding:15px">create or update an existing forwarder-evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConfigurationsConfigurationForwarderEvcsForwarderEvc(configurationId, forwarderEvcId, body, callback)</td>
    <td style="padding:15px">update an existing forwarder-evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationForwarderEvcsForwarderEvc(configurationId, forwarderEvcId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing forwarder-evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs/forwarder-evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationForwarderEvcs(configurationId, callback)</td>
    <td style="padding:15px">returns forwarder-evcs</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/forwarder-evcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationEvcsEvcRelationshipListRelationship(configurationId, evcId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationEvcsEvcRelationshipListRelationship(configurationId, evcId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationEvcsEvc(configurationId, evcId, callback)</td>
    <td style="padding:15px">returns evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfigurationEvcsEvc(configurationId, evcId, body, callback)</td>
    <td style="padding:15px">create or update an existing evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConfigurationsConfigurationEvcsEvc(configurationId, evcId, body, callback)</td>
    <td style="padding:15px">update an existing evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfigurationEvcsEvc(configurationId, evcId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing evc</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs/evc/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfigurationEvcs(configurationId, callback)</td>
    <td style="padding:15px">returns evcs</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}/evcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurationsConfiguration(configurationId, callback)</td>
    <td style="padding:15px">returns configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConfigurationsConfiguration(configurationId, body, callback)</td>
    <td style="padding:15px">create or update an existing configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConfigurationsConfiguration(configurationId, body, callback)</td>
    <td style="padding:15px">update an existing configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConfigurationsConfiguration(configurationId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing configuration</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConfigurations(configurationId, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns configurations</td>
    <td style="padding:15px">{base_path}/{version}/network/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkForwardingPathsForwardingPathRelationshipListRelationship(forwardingPathId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkForwardingPathsForwardingPathRelationshipListRelationship(forwardingPathId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkForwardingPathsForwardingPathForwardersForwarderRelationshipListRelationship(forwardingPathId, sequence, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkForwardingPathsForwardingPathForwardersForwarderRelationshipListRelationship(forwardingPathId, sequence, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkForwardingPathsForwardingPathForwardersForwarder(forwardingPathId, sequence, callback)</td>
    <td style="padding:15px">returns forwarder</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkForwardingPathsForwardingPathForwardersForwarder(forwardingPathId, sequence, body, callback)</td>
    <td style="padding:15px">create or update an existing forwarder</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkForwardingPathsForwardingPathForwardersForwarder(forwardingPathId, sequence, body, callback)</td>
    <td style="padding:15px">update an existing forwarder</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkForwardingPathsForwardingPathForwardersForwarder(forwardingPathId, sequence, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing forwarder</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders/forwarder/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkForwardingPathsForwardingPathForwarders(forwardingPathId, sequence, callback)</td>
    <td style="padding:15px">returns forwarders</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}/forwarders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkForwardingPathsForwardingPath(forwardingPathId, callback)</td>
    <td style="padding:15px">returns forwarding-path</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkForwardingPathsForwardingPath(forwardingPathId, body, callback)</td>
    <td style="padding:15px">create or update an existing forwarding-path</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkForwardingPathsForwardingPath(forwardingPathId, body, callback)</td>
    <td style="padding:15px">update an existing forwarding-path</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkForwardingPathsForwardingPath(forwardingPathId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing forwarding-path</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths/forwarding-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkForwardingPaths(forwardingPathId, forwardingPathName, callback)</td>
    <td style="padding:15px">returns forwarding-paths</td>
    <td style="padding:15px">{base_path}/{version}/network/forwarding-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkCollectionsCollectionRelationshipListRelationship(collectionId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkCollectionsCollectionRelationshipListRelationship(collectionId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCollectionsCollection(collectionId, callback)</td>
    <td style="padding:15px">returns collection</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkCollectionsCollection(collectionId, body, callback)</td>
    <td style="padding:15px">create or update an existing collection</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCollectionsCollection(collectionId, body, callback)</td>
    <td style="padding:15px">update an existing collection</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkCollectionsCollection(collectionId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing collection</td>
    <td style="padding:15px">{base_path}/{version}/network/collections/collection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCollections(collectionId, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns collections</td>
    <td style="padding:15px">{base_path}/{version}/network/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVlanTagsVlanTagRelationshipListRelationship(vlanTagId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVlanTagsVlanTagRelationshipListRelationship(vlanTagId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVlanTagsVlanTag(vlanTagId, callback)</td>
    <td style="padding:15px">returns vlan-tag</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkVlanTagsVlanTag(vlanTagId, body, callback)</td>
    <td style="padding:15px">create or update an existing vlan-tag</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVlanTagsVlanTag(vlanTagId, body, callback)</td>
    <td style="padding:15px">update an existing vlan-tag</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVlanTagsVlanTag(vlanTagId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing vlan-tag</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags/vlan-tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVlanTags(callback)</td>
    <td style="padding:15px">returns vlan-tags</td>
    <td style="padding:15px">{base_path}/{version}/network/vlan-tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConnectivitiesConnectivityRelationshipListRelationship(connectivityId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConnectivitiesConnectivityRelationshipListRelationship(connectivityId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConnectivitiesConnectivity(connectivityId, callback)</td>
    <td style="padding:15px">returns connectivity</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkConnectivitiesConnectivity(connectivityId, body, callback)</td>
    <td style="padding:15px">create or update an existing connectivity</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConnectivitiesConnectivity(connectivityId, body, callback)</td>
    <td style="padding:15px">update an existing connectivity</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkConnectivitiesConnectivity(connectivityId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing connectivity</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities/connectivity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConnectivities(connectivityId, bandwidthProfileName, vpnType, colorAware, couplingFlag, ethtSvcName, accessProviderId, accessClientId, accessTopologyId, accessNodeId, accessLtpId, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns connectivities</td>
    <td style="padding:15px">{base_path}/{version}/network/connectivities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpServiceRelationshipListRelationship(lanPortConfigId, dhcpServiceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpServiceRelationshipListRelationship(lanPortConfigId, dhcpServiceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpService(lanPortConfigId, dhcpServiceId, callback)</td>
    <td style="padding:15px">returns dhcp-service</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpService(lanPortConfigId, dhcpServiceId, body, callback)</td>
    <td style="padding:15px">create or update an existing dhcp-service</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpService(lanPortConfigId, dhcpServiceId, body, callback)</td>
    <td style="padding:15px">update an existing dhcp-service</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLanPortConfigsLanPortConfigDhcpServicesDhcpService(lanPortConfigId, dhcpServiceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing dhcp-service</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services/dhcp-service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLanPortConfigsLanPortConfigDhcpServices(lanPortConfigId, dhcpServiceId, serverIpv4Address, serverIpv6Address, dhcpServiceType, domainName, callback)</td>
    <td style="padding:15px">returns dhcp-services</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/dhcp-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLanPortConfigsLanPortConfigRelationshipListRelationship(lanPortConfigId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLanPortConfigsLanPortConfigRelationshipListRelationship(lanPortConfigId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLanPortConfigsLanPortConfig(lanPortConfigId, callback)</td>
    <td style="padding:15px">returns lan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkLanPortConfigsLanPortConfig(lanPortConfigId, body, callback)</td>
    <td style="padding:15px">create or update an existing lan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkLanPortConfigsLanPortConfig(lanPortConfigId, body, callback)</td>
    <td style="padding:15px">update an existing lan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkLanPortConfigsLanPortConfig(lanPortConfigId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing lan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs/lan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLanPortConfigs(lanPortConfigId, lanPortConfigName, deviceId, portId, ipv4Address, ipv6Address, vlanTag, callback)</td>
    <td style="padding:15px">returns lan-port-configs</td>
    <td style="padding:15px">{base_path}/{version}/network/lan-port-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNetworkResourcesNetworkResourceRelationshipListRelationship(networkId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNetworkResourcesNetworkResourceRelationshipListRelationship(networkId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetworkResourcesNetworkResource(networkId, callback)</td>
    <td style="padding:15px">returns network-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkNetworkResourcesNetworkResource(networkId, body, callback)</td>
    <td style="padding:15px">create or update an existing network-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNetworkResourcesNetworkResource(networkId, body, callback)</td>
    <td style="padding:15px">update an existing network-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNetworkResourcesNetworkResource(networkId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing network-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources/network-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetworkResources(networkId, providerId, clientId, teTopoId, callback)</td>
    <td style="padding:15px">returns network-resources</td>
    <td style="padding:15px">{base_path}/{version}/network/network-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSiteResourcesSiteResourceRelationshipListRelationship(siteResourceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSiteResourcesSiteResourceRelationshipListRelationship(siteResourceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSiteResourcesSiteResource(siteResourceId, callback)</td>
    <td style="padding:15px">returns site-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSiteResourcesSiteResource(siteResourceId, body, callback)</td>
    <td style="padding:15px">create or update an existing site-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSiteResourcesSiteResource(siteResourceId, body, callback)</td>
    <td style="padding:15px">update an existing site-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSiteResourcesSiteResource(siteResourceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing site-resource</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources/site-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSiteResources(siteResourceId, siteResourceName, type, role, generatedSiteId, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns site-resources</td>
    <td style="padding:15px">{base_path}/{version}/network/site-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSdwanVpnsSdwanVpnRelationshipListRelationship(sdwanVpnId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSdwanVpnsSdwanVpnRelationshipListRelationship(sdwanVpnId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSdwanVpnsSdwanVpn(sdwanVpnId, callback)</td>
    <td style="padding:15px">returns sdwan-vpn</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkSdwanVpnsSdwanVpn(sdwanVpnId, body, callback)</td>
    <td style="padding:15px">create or update an existing sdwan-vpn</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSdwanVpnsSdwanVpn(sdwanVpnId, body, callback)</td>
    <td style="padding:15px">update an existing sdwan-vpn</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSdwanVpnsSdwanVpn(sdwanVpnId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing sdwan-vpn</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns/sdwan-vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSdwanVpns(sdwanVpnId, sdwanVpnName, vxlanId, topology, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns sdwan-vpns</td>
    <td style="padding:15px">{base_path}/{version}/network/sdwan-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkDevicesDeviceRelationshipListRelationship(deviceId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkDevicesDeviceRelationshipListRelationship(deviceId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevicesDevice(deviceId, callback)</td>
    <td style="padding:15px">returns device</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkDevicesDevice(deviceId, body, callback)</td>
    <td style="padding:15px">create or update an existing device</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDevicesDevice(deviceId, body, callback)</td>
    <td style="padding:15px">update an existing device</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkDevicesDevice(deviceId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing device</td>
    <td style="padding:15px">{base_path}/{version}/network/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevices(deviceId, esn, deviceName, vendor, classParam, type, version, systemIp, systemIpv4, systemIpv6, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns devices</td>
    <td style="padding:15px">{base_path}/{version}/network/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkWanPortConfigsWanPortConfigRelationshipListRelationship(wanPortConfigId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkWanPortConfigsWanPortConfigRelationshipListRelationship(wanPortConfigId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkWanPortConfigsWanPortConfig(wanPortConfigId, callback)</td>
    <td style="padding:15px">returns wan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkWanPortConfigsWanPortConfig(wanPortConfigId, body, callback)</td>
    <td style="padding:15px">create or update an existing wan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkWanPortConfigsWanPortConfig(wanPortConfigId, body, callback)</td>
    <td style="padding:15px">update an existing wan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkWanPortConfigsWanPortConfig(wanPortConfigId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing wan-port-config</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs/wan-port-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkWanPortConfigs(wanPortConfigId, wanPortConfigName, deviceId, ipAddress, ipv4Address, ipv6Address, portType, portNumber, devicePortId, wanPortId, operationalStatus, modelInvariantId, modelVersionId, callback)</td>
    <td style="padding:15px">returns wan-port-configs</td>
    <td style="padding:15px">{base_path}/{version}/network/wan-port-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfoRelationshipListRelationship(aaiId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfoRelationshipListRelationship(aaiId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfo(aaiId, esrSystemInfoId, callback)</td>
    <td style="padding:15px">returns esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfo(aaiId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">create or update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfo(aaiId, esrSystemInfoId, body, callback)</td>
    <td style="padding:15px">update an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkExtAaiNetworksExtAaiNetworkEsrSystemInfo(aaiId, esrSystemInfoId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing esr-system-info</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/esr-system-info/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkExtAaiNetworksExtAaiNetworkRelationshipListRelationship(aaiId, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkExtAaiNetworksExtAaiNetworkRelationshipListRelationship(aaiId, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkExtAaiNetworksExtAaiNetwork(aaiId, callback)</td>
    <td style="padding:15px">returns ext-aai-network</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateNetworkExtAaiNetworksExtAaiNetwork(aaiId, body, callback)</td>
    <td style="padding:15px">create or update an existing ext-aai-network</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkExtAaiNetworksExtAaiNetwork(aaiId, body, callback)</td>
    <td style="padding:15px">update an existing ext-aai-network</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkExtAaiNetworksExtAaiNetwork(aaiId, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing ext-aai-network</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks/ext-aai-network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkExtAaiNetworks(aaiId, callback)</td>
    <td style="padding:15px">returns ext-aai-networks</td>
    <td style="padding:15px">{base_path}/{version}/network/ext-aai-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCommonContactsContactRelationshipListRelationship(contactName, body, callback)</td>
    <td style="padding:15px">see node definition for valid relationships</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommonContactsContactRelationshipListRelationship(contactName, callback)</td>
    <td style="padding:15px">delete an existing relationship</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}/relationship-list/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommonContactsContact(contactName, callback)</td>
    <td style="padding:15px">returns contact</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateCommonContactsContact(contactName, body, callback)</td>
    <td style="padding:15px">create or update an existing contact</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCommonContactsContact(contactName, body, callback)</td>
    <td style="padding:15px">update an existing contact</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommonContactsContact(contactName, resourceVersion, callback)</td>
    <td style="padding:15px">delete an existing contact</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts/contact/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommonContacts(callback)</td>
    <td style="padding:15px">returns contacts</td>
    <td style="padding:15px">{base_path}/{version}/common/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
